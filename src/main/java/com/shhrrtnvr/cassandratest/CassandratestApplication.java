package com.shhrrtnvr.cassandratest;

import com.shhrrtnvr.cassandratest.model.Car;
import com.shhrrtnvr.cassandratest.repository.CarRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
@Slf4j
public class CassandratestApplication {

  public static void main(String[] args) {
    SpringApplication.run(CassandratestApplication.class, args);
  }

//  @Bean
//  public CommandLineRunner commandLineRunner(CarRepository carRepository) {
//    return args -> {
//      var myCar = new Car()
//          .setPk(new Car.Key().setManufacturer("BMW").setChassisNumber("123"))
//              .setOwner("ST");
//      carRepository.save(myCar);
//      carRepository.findById(new Car.Key().setManufacturer("BMW").setChassisNumber("123"))
//          .ifPresent(car -> log.info("owner: {}, manufacturer: {}", car.getOwner(), car.getPk().getManufacturer()));
//    };
//  }
}
