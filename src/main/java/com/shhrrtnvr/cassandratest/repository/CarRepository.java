package com.shhrrtnvr.cassandratest.repository;

import com.shhrrtnvr.cassandratest.model.Car;
import org.springframework.data.cassandra.repository.CassandraRepository;

public interface CarRepository extends CassandraRepository<Car, Car.Key> {
}
