package com.shhrrtnvr.cassandratest.chat;

import com.corundumstudio.socketio.*;
import com.corundumstudio.socketio.listener.DataListener;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

import java.util.UUID;

@Component
public class AckChatLauncher {

    @Bean
    public CommandLineRunner commandLineRunner2(){
        return args -> {
            Configuration config = new Configuration();
            config.setHostname("localhost");
            config.setPort(9092);

            final SocketIOServer server = new SocketIOServer(config);
            server.addEventListener("ackevent1", ChatObject.class, new DataListener<ChatObject>() {
                @Override
                public void onData(final SocketIOClient client, ChatObject data, final AckRequest ackRequest) {

                    // check is ack requested by client,
                    // but it's not required check
                    if (ackRequest.isAckRequested()) {
                        // send ack response with data to client
                        ackRequest.sendAckData("client message was delivered to server!", "yeah!");
                    }

                    // send message back to client with ack callback WITH data
                    ChatObject ackChatObjectData = new ChatObject(data.getUserName(), "message with ack data");
                    client.sendEvent("ackevent2", new AckCallback<String>(String.class) {
                        @Override
                        public void onSuccess(String result) {
                            System.out.println("ack from client: " + client.getSessionId() + " data: " + result);
                        }
                    }, ackChatObjectData);

                    ChatObject ackChatObjectData1 = new ChatObject(data.getUserName(), "message with void ack");
                    client.sendEvent("ackevent3", new VoidAckCallback() {

                        protected void onSuccess() {
                            System.out.println("void ack from: " + client.getSessionId());
                        }

                    }, ackChatObjectData1);
                }
            });

            server.addEventListener("msg", byte[].class, new DataListener<byte[]>() {
                @Override
                public void onData(SocketIOClient client, byte[] data, AckRequest ackRequest) {
                    client.sendEvent("msg", data);
                }
            });

            server.addEventListener("chatevent", ChatObject.class, new DataListener<ChatObject>() {
                @Override
                public void onData(SocketIOClient client, ChatObject data, AckRequest ackRequest) {
                    // broadcast messages to all clients
                    server.getBroadcastOperations().sendEvent("chatevent", data);
                }
            });


            final SocketIONamespace chat1namespace = server.addNamespace("/chat1");
            chat1namespace.addEventListener("message", ChatObject.class, new DataListener<ChatObject>() {
                @Override
                public void onData(SocketIOClient client, ChatObject data, AckRequest ackRequest) {
                    // broadcast messages to all clients
                    chat1namespace.getBroadcastOperations().sendEvent("message", data);
                }
            });

            final SocketIONamespace chat2namespace = server.addNamespace("/chat2");
            chat2namespace.addEventListener("message", ChatObject.class, new DataListener<ChatObject>() {
                @Override
                public void onData(SocketIOClient client, ChatObject data, AckRequest ackRequest) {
                    // broadcast messages to all clients
                    chat2namespace.getBroadcastOperations().sendEvent("message", data);
                }
            });


            server.start();

            Thread.sleep(Integer.MAX_VALUE);

            server.stop();
        };
    }

}
